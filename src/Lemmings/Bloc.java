package Lemmings;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

public abstract class Bloc extends JPanel{
	
	//Le Monde est constitue de blocs de taille 20x20
	//Chaque bloque possede une image
	//Les Blocs fixes sont positionnes sur la grille du monde
	//Les Blocs mobiles ne sont pas ajoutes sur la grille du monde
	//Les blocs mobiles sont annimes par un animeur et se deplacent dans le monde
	
	
	protected Monde monde; //Monde dans lequel le bloc est ajoute
	protected int posX; //Position X a la creation du bloc
	protected int posY; //Position Y a la creation du bloc
	protected Image image; //Image du bloc 
	
	public int getPosX(){
		return posX;
	}
	
	public int getPosY(){
		return posY;
	}
	
	public void animer(){
		//Il s'agit de la methode qui anime un bloc
		//Suivant le role du lemmings l'animation est differente
		//Cette methode est appelee par le thread animateur du bloc
	}
	
	public void vitesse(int v){
		//Permet d'ajuster la freq du Sleep de l'animateur du bloc.
		//Cela permet de ralentir ou accelerer l'animation du bloc
	}
	
	public void placer(){
		//cette methode affiche le bloc sur la fenetre.
		this.setBounds(posY*20, posX*20, 20, 20);
	}
	
	@Override
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		g.drawImage(image, 0, 0, this);
	}
}
