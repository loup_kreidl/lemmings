package Lemmings;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class BarreBoutons extends JPanel implements ActionListener {
	
	//C'est la barre a droite de l'ecran qui permet de selectionner l'action de chaque lemming 
	
	Monde monde;
	JTextField nbLemming;
	List<JRadioButton> boutons;
	
	public BarreBoutons(Monde m, JTextField cpt){
		monde = m;
		this.setSize(100,600);
		boutons = new ArrayList<JRadioButton>();
		
		//boutons d'action
		boutons.add(new JRadioButton("Grimpeur"));
		boutons.add(new JRadioButton("Parachutiste"));
		boutons.add(new JRadioButton("Bloqueur"));
		boutons.add(new JRadioButton("Exploseur"));
		boutons.add(new JRadioButton("Constructeur"));
		boutons.add(new JRadioButton("Pelleteur"));
		boutons.add(new JRadioButton("Foreur"));
		boutons.add(new JRadioButton("Mineur"));
		
		//ajout du listener pour chaque bouton
		for(JRadioButton b : boutons)
			b.addActionListener(this);

		
		//ajout des bouton dans un group de selection "unique"
		ButtonGroup actions = new ButtonGroup();
		for(JRadioButton b : boutons)
			actions.add(b);
		
		JPanel barre = new JPanel(new GridLayout(0,1));
		for(JRadioButton b : boutons)
			barre.add(b);

		nbLemming = cpt;
		barre.add(nbLemming);
		add(barre, BorderLayout.LINE_START);
	}

	public void actionPerformed(ActionEvent arg0) {
		System.out.println("dfdfg");
		
	}
}
