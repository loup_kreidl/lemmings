package Lemmings;

import java.awt.BorderLayout;
import javax.swing.*;

public class Main {
	public static void main(String[] args) { 
		
		//Fenetre principale
		JFrame fenetre = new JFrame("Lemmings");
		fenetre.setSize(1000, 600);
		fenetre.setLayout(new BorderLayout());
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//Compteur de lemmings
		JTextField compteur = new JTextField("0/10 Lemming");
		
		//Creation du monde avec le niveau en parametre
		Monde m = new Monde(1,compteur);
		
		BarreBoutons barre = new BarreBoutons(m,compteur);
		fenetre.add(m,BorderLayout.CENTER);
		fenetre.add(barre,BorderLayout.EAST);
		fenetre.setVisible(true);
		
	}
}
