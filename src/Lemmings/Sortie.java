package Lemmings;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Sortie extends Bloc{
	
	//C'est la sortie du monde des Lemmings
	
	private File f1;
	private int nbLemming;
	public Sortie(Monde m, int x, int y){
		this.posX = x;
		this.posY = y;
		this.monde = m;
		this.nbLemming = 10;
		this.setSize(20,20);
		this.setOpaque(false);
		try {
			this.f1 = new File("images/sortie.png");
			image = ImageIO.read(f1);
		} catch (IOException e) {
			e.printStackTrace();
		}
		monde.add(this);
		placer(); //methode classe Bloc
	}
}