package Lemmings;

import java.awt.Color;
import java.io.*;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Monde extends JPanel{
	
	//Un monde est un Jpanel ou des blos sont affich�s et peuvent se d�placer
	//Un monde possede des blocs Bloc_Terre, Depart et Sortie.
	//Le monde est compos� d'une grille qui contient les blocs fixes.
	//Cela permet de faire des tests lors des deplacements des lemmings dans leur environnement
	
	private String role; //role s�lectionn� � la souris
	private int niveau;
	private Bloc grille[][];
	private JTextField panelCompteur; //Affichage du compteur de leimmings
	private int compteur; //compteur de lemmings arrive a la sortie
	
	public Monde(int niveau, JTextField panelCompteur){
		compteur = 0;
		this.panelCompteur = panelCompteur;
		this.niveau = niveau;
		this.setSize(900,600);
		this.setLayout(null);
		this.setBackground(Color.BLACK);
		grille = new Bloc[30][45];
		File f = new File("niveaux/niv"+this.niveau);//fichier bin qui contient la matrice du monde
		chargerNiveau(f);
	}
	
	public void chargerNiveau(File f){
		InputStream in = null;
		try {
			in = new FileInputStream(f);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	    Reader lecteur = new InputStreamReader(in);
	    int c;
	    try {
	    	for(int i=0; i<30; i++){
				for(int j=0; j<45; j++){
					c = lecteur.read(); //lecture du fichier caractere par caractere, ligne par ligne
					switch (c) {
					case 10:
						c = lecteur.read();
						break;
					case 49:
						grille[i][j] = new Bloc_Terre(this,i,j);
						break;
					case 50:
						grille[i][j] = new Depart(this,i,j);
						break;
					case 51:
						grille[i][j] = new Sortie(this,i,j);
						break;
					default:
						break;
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public Bloc typeBloc(int x, int y){
		return grille[x][y];
	}

	public Bloc[][] getGrille(){
		return grille;
	}
	
	
	public void supprimerBloc(Bloc b){
		grille[b.getPosX()][b.getPosY()] = null;
		this.remove(b);
		this.revalidate();
		this.repaint();
		
	}
	public void incrementer(){
		//Methode qui incremente le compteur de lemming arrives a la sortie
		compteur++;
		panelCompteur.setText(compteur+"/10 Lemming");
	}
}
