package Lemmings;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;


public class Bloc_Terre extends Bloc{
	
	//Un bloc de terre empeche les lemmings de passer.
	//Il s'agit d'un bloc fixe qui peut �tre detruit par certain lemmings
	
	public Bloc_Terre(Monde m, int x, int y){
		this.posX = x;
		this.posY = y;
		this.monde = m;
		this.setSize(20,20);
		this.setVisible(true);
		image = null;
		try {
			File f = new File("images/terre.png");
			image = ImageIO.read(f);
		} catch (IOException e) {
			e.printStackTrace();
		}
		monde.add(this);
		placer(); //methode classe Bloc
	}
}
