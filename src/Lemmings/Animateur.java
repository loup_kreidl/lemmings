package Lemmings;

public class Animateur extends Thread {
	
	//Cet objet anime tous les blocs qui sont animes.
	//La tache est executee dans un thread.
	//Il fait appel a la fonction Bloc.anime()
	//Le thread est interrompu x secondes dans la boucle pour ralentir l'animation
	
	private Bloc bloc; //bloc a animer
	private int freq; //temps du sleep
	private boolean vivant; //etat du bloc, mort si arriv� a la sortie
	public Animateur(Bloc b){
		bloc = b;
		freq = 200;
		vivant = true;
	}
	public void run(){
		while(vivant){
			bloc.animer();
			try {
				Thread.sleep(freq);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		}
	}
	public void setVitesse(int v){
		freq = v;
	}
	public void tuer(){
		vivant = false;
	}
}
