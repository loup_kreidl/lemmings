package Lemmings;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Lemming extends Bloc{
	
	//Un Lemming est un bloc qui se deplace de gauche a droite sur le sol
	//Pour marcher, un animateur fait appel a sa methode animer()
	//Si un Lemming arrive sur la sortie il est supprime.
	
	private int direction; //sens de deplacement du marcheur : gauche=1 et droite=0
	private File f1,f2;
	private Animateur anime;
	private boolean chute;
	private int cpt_chute;
	public Lemming(Monde m, int x, int y){
		this.posX = x;
		this.posY = y;
		this.monde = m;
		this.setSize(20,20);
		this.setOpaque(false);
		direction = 0;
		chute = false;
		cpt_chute = 0;
		try {
			this.f1 = new File("images/lemmings_gifs/walkr_x2.gif"); //sens droite
			this.f2 = new File("images/lemmings_gifs/walkl_x2.gif"); //sens gauche
			image = ImageIO.read(f1);
		} catch (IOException e) {
			e.printStackTrace();
		}
		//ajout du bloc dans le monde et affichage
		monde.add(this);
		placer(); //methode de Bloc
		anime = new Animateur(this);
		anime.setVitesse(500);
		anime.start();
	}

	
	public void animer(){
		//s'il y a de la terre sous le marcheur
		if((monde.typeBloc(posX+1, posY) instanceof Bloc_Terre)){
			//s'il peut aller vers la droite 
			if(cpt_chute > 4){
				chute = true;
			}
			cpt_chute = 0;
			if(!(monde.typeBloc(posX, posY+1) instanceof Bloc_Terre) && direction==0){
				setImage(f1);
				testSortie(posX,posY+1);
				this.posY++;
				placer();
			}else{
				//sinon il va vers la gauche
				direction=1;
				if(!(monde.typeBloc(posX, posY-1) instanceof Bloc_Terre) && direction==1){
					setImage(f2);
					testSortie(posX,posY-1);
					this.posY--;
					placer();
				}else{
					direction--;
				}
			}
		}else{
			//sinon il tombe vers le bas
			cpt_chute++;
			testSortie(posX+1,posY);
			posX++;
			placer();
		}
	}
	
	public void testSortie(int x, int y){
		//Cette methode teste si le bloc dans lequel le marcheur va se deplacer est la sortie
		if(chute){
			System.out.println("chute");
			anime.tuer();
			monde.supprimerBloc(this);
		}
		if((monde.typeBloc(x,y) instanceof Sortie)){
			System.out.println("sortie");
			anime.tuer();
			monde.supprimerBloc(this);
			monde.incrementer();
		}
	}
	
	public void setImage(File f){
			try {
				image = ImageIO.read(f);
			} catch (IOException e) {
				e.printStackTrace();
			}
	}
	
	public void vitesse(int v){
		anime.setVitesse(v);
	}
}
