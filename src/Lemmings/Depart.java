package Lemmings;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Depart extends Bloc{
	
	//C'est le bloc de depart d'ou sortent les lemmings en lan�ant le jeux.
	//L'animeteur permet de faire sortir les lemmings a interval regulier.
	
	private Animateur anime;
	private File f;
	private int nbLemmings;
	public Depart(Monde m, int x, int y){
		this.posX = x;
		this.posY = y;
		this.monde = m;
		this.nbLemmings = 10;
		this.setSize(20,20);
		this.setOpaque(false);
		try {
			this.f = new File("images/depart.png");
			image = ImageIO.read(f);
		} catch (IOException e) {
			e.printStackTrace();
		}
		monde.add(this);
		placer(); //methode classe Bloc
		
		//Creation de l'animateur du bloc
		anime = new Animateur(this);
		anime.setVitesse(1000);
		anime.start();
	}
	
	public void animer(){
		if(nbLemmings > 0){
			new Lemming(monde,posX,posY);
			nbLemmings--;
		}
	}
}
